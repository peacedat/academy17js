const logo = document.querySelector('.logo');
const menu = document.querySelector('.menu');
const menuItems = document.querySelectorAll('.item')
const content = document.querySelector('.content h1')

logo.addEventListener('click', function () {
    menu.classList.toggle('open')
})

menu.addEventListener('click', function (event) {
    if (this === event.target)
        menu.classList.remove('open')
})

menuItems.forEach(item => {
    item.addEventListener('click', function () {
        const text = this.querySelector('.p-10').innerText
        content.innerText = text
    })
})
